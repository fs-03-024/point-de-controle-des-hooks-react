import React from 'react';

function MovieFilter({ onFilterTitle, onFilterRating }) {
  const handleTitleFilter = (e) => {
    onFilterTitle(e.target.value);
  };

  const handleRatingFilter = (e) => {
    onFilterRating(e.target.value);
  };

  return (
    <div>
      <label>
        Title:
        <input type="text" onChange={handleTitleFilter} />
      </label>
      <label>
        Rating:
        <input type="number" min="0" max="10" onChange={handleRatingFilter} />
      </label>
    </div>
  );
}

export default MovieFilter;