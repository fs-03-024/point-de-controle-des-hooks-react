import React, { useState } from 'react';
import './App.css';
import MovieList from './components/MovieList';
import MovieForm from './components/MovieForm';
import MovieFilter from './components/MovieFilter';

function App() {
  const [movies, setMovies] = useState([
    {
      id: 1,
      title: 'The Shawshank Redemption',
      description: 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
      poster: 'https://m.media-amazon.com/images/M/MV5BNjQ2NDA3MDcxMF5BMl5BanBnXkFtZTgwMjE5NTU0NzE@._V1_SX300.jpg',
      rating: 9.3
    },
    {
      id: 2,
      title: 'The Godfather',
      description: 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
      poster: 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg',
      rating: 9.2
    },
    // Add more movies here
  ]);

  const [filterTitle, setFilterTitle] = useState('');
  const [filterRating, setFilterRating] = useState(0);

  const addMovie = (newMovie) => {
    setMovies([...movies, newMovie]);
  };

  return (
    <div>
      <h1>My Movie Collection</h1>
      <MovieForm onAddMovie={addMovie} />
      <MovieFilter
        onFilterTitle={setFilterTitle}
        onFilterRating={setFilterRating}
      />
      <MovieList
        movies={movies.filter(
          (movie) =>
            movie.title.toLowerCase().includes(filterTitle.toLowerCase()) &&
            movie.rating >= filterRating
        )}
      />
    </div>
  );
}

export default App;